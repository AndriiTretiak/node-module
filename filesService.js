const fs = require("fs");
const express = require("express");
const morgan = require("morgan");
const app = express();

const path = require("path");

app.use(express.json());
app.use(morgan("tiny"));

function createFile(req, res) {
  if (!req.body || !req.body.filename || !req.body.content) {
    return res
      .status(400)
      .send({ message: "Please specify 'content' parameter" });
  }
  fs.writeFile(`./files/${req.body.filename}`, req.body.content, (err) => {
    if (err) return res.status(500).send({ message: "Server error" });
  });
  res.status(200).send({ message: "File created successfully" });
}

function getFiles(req, res) {
  fs.readdir("./files/", (err, files) => {
    if (err) {
      return res.status(500).send({
        message: "Server error",
      });
    }
    if (files.length == 0) {
      return res.status(400).send({
        message: "Client error",
      });
    }
    res.status(200).send({
      message: "Success",
      files: files,
    });
  });
}

const getFile = (req, res) => {
  if (!fs.existsSync(`./files/${req.params.filename}`)) {
    return res.status(400).send({
      message: `No file with ${req.params.filename} filename found`,
    });
  }
  const date = fs.statSync(`./files/${req.params.filename}`).mtime;
  const data = fs.readFileSync(`./files/${req.params.filename}`, {
    encoding: "utf8",
    flag: "r",
  });
  res.status(200).send({
    message: "Success",
    filename: req.params.filename,
    content: data,
    extension: req.params.filename.split(".").pop(),
    uploadedDate: date,
  });
};

const editFile = (req, res) => {
  const curContent = fs.readFileSync(`./files/${req.body.filename}`, {
    encoding: "utf8",
    flag: "r",
  });
  if (req.body.content == curContent || !req.body.content) {
    return res.status(400).send({ message: "Client error" });
  }
  fs.writeFile(`./files/${req.body.filename}`, req.body.content, (err) => {
    if (err) return res.status(500).send({ message: "Server error" });
  });
  res.status(200).send({ message: "File edited successfully" });
};

const deleteFile = (req, res, next) => {
  fs.unlink(`./files/${req.body.filename}`, (err) => {
    if (err) return res.status(500).send({ message: "Server error" });
  });
  res.status(200).send({ message: "File deleted" });
};

module.exports = {
  createFile,
  getFiles,
  getFile,
  editFile,
  deleteFile,
};
